private void httpget() {

 //快递公司解析
        String typeUrL = urlString + "type" + "?appkey=" + APPKEY;
        StringRequest typeRequest = new StringRequest(typeUrL, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject object = new JSONObject(s);
                    //解析状态码
                    String status = object.getString("status");
                    if (status != null && status.equals("0")) {
                        JSONArray result = object.getJSONArray("result");
                        int size = result.length();
                        for (int i = 0; i < size; i++) {
                            JSONObject object2 = result.getJSONObject(i);
                            //方法1
//                            String type=object2.getString("type");
//                            String name=object2.getString("name");
                            //方法2
                            ExpressTypeDb typeDb = new ExpressTypeDb();
                            typeDb.expressType = object2.getString("type");
                            typeDb.expressName = object2.getString("name");
                            ifExitType(typeDb.expressType);//查重
                            android.util.Log.i("maty", typeDb.expressName);

                            typeDb.save();
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        });
        BaseApp.GetQueue().add(typeRequest);
}	