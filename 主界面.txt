
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:id="@+id/activity_main"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:orientation="vertical"
    tools:context="com.example.haoge.study_expressage.MainActivity">


    <TextView
        android:id="@+id/title"
        android:layout_width="match_parent"
        android:layout_height="70dp"
        android:background="#eeeeff"
        android:gravity="center"
        android:text="随便查"
        android:textColor="#000000"
        android:textSize="25dp" />

    <ImageView
        android:layout_width="100dp"
        android:layout_height="100dp"
        android:layout_gravity="center"
        android:layout_margin="20dp"
        android:src="@mipmap/kdlogo" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="50dp"
        android:layout_marginTop="10dp"
        android:orientation="horizontal">

        <ImageView
            android:id="@+id/iv_saomiao"
            android:layout_width="40dp"
            android:layout_height="40dp"
            android:layout_weight="0.2"
            android:src="@mipmap/saomiao2" />

        <EditText
            android:id="@+id/edt_number"
            android:layout_width="200dp"
            android:layout_height="40dp"
            android:layout_weight="0.6"
            android:background="@drawable/backgound"
            android:gravity="center"
            android:hint="请输入快递单号" />

        <ImageView
            android:id="@+id/yuyin"
            android:layout_width="40dp"
            android:layout_height="40dp"
            android:layout_marginRight="10dp"
            android:layout_weight="0.2"
            android:src="@mipmap/yuying2" />
    </LinearLayout>

    <Button
        android:id="@+id/btn_search"
        android:layout_width="140dp"
        android:layout_height="40dp"
        android:layout_gravity="center_horizontal"
        android:layout_marginTop="20dp"
        android:background="@drawable/shape_btn"
        android:text="查询" />

    <Button
        android:id="@+id/btn_history"
        android:layout_width="140dp"
        android:layout_height="40dp"
        android:layout_gravity="center_horizontal"
        android:layout_marginTop="20dp"
        android:background="@drawable/shape_btn"
        android:text="历史" />

    <Button
        android:id="@+id/btn_exit"
        android:layout_width="140dp"
        android:layout_height="40dp"
        android:layout_gravity="center_horizontal"
        android:layout_marginTop="20dp"
        android:background="@drawable/shape_btn"
        android:text="退出" />

</LinearLayout>
