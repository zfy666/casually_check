package com.example.haoge.study_expressage;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

/**
 * Created by haoge on 2017/9/25.
 */
//快递轨迹界面
public class TrackActivity  extends Activity implements Serializable {
    private ListView  track_list;
    private Context mcontext;
    private   TrackAdapter  trackAdapter;
    private ArrayList<Track_Bean>  mArraylist;
    private   String expressNum;
    private   ExpressDb db;
    private TextView mTitle;//快递公司初始化
    private ImageView mLogo;//快递公司logo显示
    private String type;
    private ExpressTypeDb typeDb;

    android.os.Handler handler=new android.os.Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(msg.what==0x001){
                mTitle.setText(typeDb.expressName);
                mLogo.setImageResource(msg.arg1);
            }

        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.logistics);
        mTitle= (TextView) findViewById(R.id.track_mTitle);
        mLogo= (ImageView) findViewById(R.id.track_mLogo);

        mcontext=this;
        //db=new ExpressDb();
        track_list=(ListView) findViewById(R.id.list_logistics);
        mArraylist=new ArrayList<Track_Bean>();

        Intent it =getIntent();
        expressNum=it.getStringExtra("number");
        db=new Select().from(ExpressDb.class).where("number=?",expressNum).executeSingle();
   //     db.resultJson;
           ExpressBean expressBean=new Gson().fromJson(db.resultJson,ExpressBean.class);
         mArraylist=expressBean.list;

        //获取快递公司代号Type
        type=expressBean.type;
        typeDb=new Select().from(ExpressTypeDb.class)
                .where("type=?",expressBean.type.toUpperCase()).executeSingle();
        //快递公司名字
        Message message=handler.obtainMessage();
        message.what=0x001;
        message.obj=typeDb.expressName;
        int resID = getResources().getIdentifier(type, "mipmap",
                getPackageName());
        message.arg1=resID;
        handler.sendMessage(message);

        trackAdapter=new TrackAdapter(mcontext,mArraylist);
        track_list.setAdapter(trackAdapter);
    }
}
