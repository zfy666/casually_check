package com.example.haoge.study_expressage.Activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;
import com.example.haoge.study_expressage.Bean.ExpressBean;
import com.example.haoge.study_expressage.Bean.ExpressDb;
import com.example.haoge.study_expressage.Bean.History_Bean;
import com.example.haoge.study_expressage.MyAdapter.HistoryAdapter;
import com.example.haoge.study_expressage.R;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class HistoryActivity extends Activity implements Serializable {
    private  ListView his_listview;
    private Context mcontext;
    private HistoryAdapter historyAdapter;
    private List<ExpressDb>  history_been;
    private ExpressDb db=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        mcontext=this;
        his_listview=(ListView) findViewById(R.id.list_history);

        history_been  =new ArrayList<ExpressDb>();
        history_been=new Select().from(ExpressDb.class).execute();



//        db = new Select().from(ExpressDb.class).where("bz=?", beizhuNanme).executeSingle();
//        ExpressBean hisBean = new Gson().fromJson(db.beizhuName, ExpressBean.class);
//        history_been = hisBean.his_list;

        //收到来自被主界面的值
        Intent beizhu=getIntent();
        String beizhuNanme=beizhu.getStringExtra("beizhu");

        historyAdapter=new HistoryAdapter(mcontext,history_been,beizhuNanme);
        his_listview.setAdapter(historyAdapter);

        his_listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent  it=new Intent(mcontext,TrackActivity.class);
                it.putExtra("number",history_been.get(position).expressNum);
                startActivity(it);
            }
        });

        //长按删除
        his_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                //定义AlertDialog.Builder对象，当长按列表项的时候弹出确认删除对话框，确认是否需要删除
                AlertDialog.Builder builder=new AlertDialog.Builder(HistoryActivity.this);
                builder.setMessage("确定删除?");
                builder.setTitle("提示");

                //添加AlertDialog.Builder对象的setPositiveButton()方法
                //即对话框中点击“确定”后发生的事件===》删除记录
                builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(history_been.remove(position)!=null){
                            System.out.println("success");
                        }else {
                            System.out.println("failed");
                        }
                        historyAdapter.notifyDataSetChanged();
                        Toast.makeText(getBaseContext(), "删除列表项", Toast.LENGTH_SHORT).show();
                    }
                });

                //添加AlertDialog.Builder对象的setNegativeButton()方法
                //即对话框中点击“取消”后发生的事件====》退出对话框
                builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                builder.create().show();
                return false;
            }
        });
    }
}
