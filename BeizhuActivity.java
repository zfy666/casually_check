package com.example.haoge.study_expressage.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import android.widget.TextView;


import com.example.haoge.study_expressage.Bean.ExpressDb;
import com.example.haoge.study_expressage.R;

/**
 * Created by Administrator on 2018/5/26.
 */

public class BeizhuActivity extends Activity implements View.OnClickListener {
    private TextView num;
    protected EditText ed_name;
    private Button btnSave,btnExit;


    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beizhu);

        num= (TextView) findViewById(R.id.beizhu_num);
        ed_name= (EditText) findViewById(R.id.edt_beizhu);
        btnSave= (Button) findViewById(R.id.beizhu_save);
        btnExit= (Button) findViewById(R.id.beizhu_exit);
        btnExit.setOnClickListener(this);
        btnSave.setOnClickListener(this);
        Intent it1 =getIntent();
        num.setText(it1.getStringExtra("number"));

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.beizhu_exit:
                finish();
                break;
            case R.id.beizhu_save:
                String name= ed_name.getText().toString().trim();
                Intent it = new Intent(this, HistoryActivity.class);
                startActivity(it);
                break;

    }
    }
}
