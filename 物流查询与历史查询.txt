//��ʷ����������

package com.example.haoge.study_expressage;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by haoge on 2017/9/25.
 */

public class  HistoryAdapter  extends BaseAdapter {
    private Context mcontext;
    private List<ExpressDb> history_list;
    public    HistoryAdapter (Context  mcontext, List<ExpressDb> history_list){
        super();
        this.mcontext=mcontext;
      this.history_list=history_list;
    }

    @Override
    public int getCount() {
        return history_list.size();
    }

    @Override
    public Object getItem(int position) {
        return history_list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      ViewHolder  holder;
        if(convertView==null) {
            holder  =new ViewHolder();
            convertView= LayoutInflater.from(mcontext).inflate(R.layout.history_list,null);
            convertView.setTag(holder);
        }else{
            holder=(ViewHolder) convertView.getTag();
        }
        holder.his_num=(TextView) convertView.findViewById(R.id.his_num);

       ExpressDb his_bean = history_list.get(position);
        holder.his_num.setText(his_bean.expressNum);

        return convertView;
    }
    class  ViewHolder{

        TextView his_num;


    }
}
